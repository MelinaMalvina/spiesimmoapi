<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;






/**
 * @ApiResource(
 *     collectionOperations={"get"},
 *     itemOperations={"get"},
 *   
 * )
 */
class Dependency
{

    /**
     * @ApiProperty(identifier=true)
     */
    private string $uuid;
    /**
     * @ApiProperty(description="Nom de la dépendance")
     */
    private string $name;
    /**
     * @ApiProperty(description="version de la dépendance")
     */
    private string $version; 

    public function __construct(
        string $uuid,
        string $name,
        string $version

    )
    {
       $this->uuid = $uuid;
       $this->name = $name;
       $this->version = $version;
    }
}